// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#ifndef __ATHWART_H__
#define __ATHWART_H__

#include "EngineMinimal.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAthwart, Log, All);


#endif
