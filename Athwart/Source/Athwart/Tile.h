// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Tile.generated.h"

UCLASS()
class ATHWART_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void SetAsTrap(float time, float offset);
	void SetAsTrap(float time, float offset, float delay);
	void SetAsWall();
	void SetAsEmpty();

	void SetIndex(int index){ iIndex = index; }
	int GetIndex() { return iIndex; }

	bool isTrap;

private:
	
	void Offset();
	void OffsetWithDelay();
	void Delay();
	void Explode();
	void ExplodeWithDelay();
	void ResetTile();
	void ResetIsTrap();

	int iIndex;
	float fTrapTime;
	float fDelayTime;
	class ATrap* tTrap;
	FTimerHandle thTime;
	FTimerHandle thDelay;
	
};
