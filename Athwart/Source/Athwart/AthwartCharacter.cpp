// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Athwart.h"
#include "AthwartCharacter.h"
#include "Tile.h"
#include "Trap.h"
#include "EngineUtils.h"
#include "TileManager.h"

AAthwartCharacter::AAthwartCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->AttachTo(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

}

void AAthwartCharacter::BeginPlay()
{
	Super::BeginPlay();
	numStage = 1;
	numDeath = 0;
	TM = GetWorld()->SpawnActor<ATileManager>(ATileManager::StaticClass());
}

void AAthwartCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (GetActorLocation().X > 800){
		numDeath++;
		NextStage();
	}
	//OnActorHit.AddDynamic(this, &AAthwartCharacter::OnMyActorHit);
}


void AAthwartCharacter::NextStage(){
	SetActorLocation(FVector(-1400.0f, -60.0f, 375.0f));
	numStage++;

	switch (numStage){
	case 2:
		TM->PresetTwo(2.0f);
		break;
	case 3:
		TM->PresetThree(4.0f);
		break;
	case 4:
		TM->PresetFour(2.0f);
		break;
	}
}