// Fill out your copyright notice in the Description page of Project Settings.

#include "Athwart.h"
#include "Tile.h"
#include "Trap.h"
#include "AthwartCharacter.h"


// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetActorEnableCollision(true);
	isTrap = false;
	fTrapTime = 0;
	fDelayTime = 0;
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	//tTrap = GetWorld()->SpawnActor<ATrap>();
}

// Called every frame
void ATile::Tick( float DeltaTime )
{
	Super::Tick(DeltaTime);
}

void ATile::SetAsTrap(float time, float offset){
	SetAsEmpty();
	fTrapTime = time;
	fDelayTime = 0;
	isTrap = true;

	if (offset)
		GetWorldTimerManager().SetTimer(thTime, this, &ATile::Explode, offset, false);
	else
		Explode();
}
/*
void ATile::SetAsTrap(float time, float offset, float delay){
	SetAsEmpty();
	fTrapTime = time;
	fDelayTime = delay;
	if (offset)
		GetWorldTimerManager().SetTimer(thTime, this, &ATile::OffsetWithDelay, offset, false);
	else
		GetWorldTimerManager().SetTimer(thTime, this, &ATile::ExplodeWithDelay, fTrapTime, false);
}
*/
void ATile::SetAsWall(){
	//move 90cm up
	SetAsEmpty();
	SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, 280.0f));
}

void ATile::SetAsEmpty(){

	GetWorldTimerManager().ClearTimer(thTime);
	GetWorldTimerManager().ClearTimer(thDelay);

	isTrap = false;
	fTrapTime = 0;
	fDelayTime = 0;
	SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, 230.0f));
}

/*
void ATile::OffsetWithDelay()
{
	GetWorldTimerManager().SetTimer(thTime, this, &ATile::ExplodeWithDelay, fTrapTime, true);
}

void ATile::Delay()
{
	GetWorldTimerManager().SetTimer(thTime, this, &ATile::ExplodeWithDelay, fTrapTime, false);
}
*/

void ATile::Explode(){

	if (isTrap){
		TArray<AActor*> OverlappingActors;
		GetOverlappingActors(OverlappingActors);
		for (int i = 0; i < OverlappingActors.Num(); i++){
			if (Cast<AAthwartCharacter>(OverlappingActors[i]) != nullptr){
				OverlappingActors[i]->SetActorLocation(FVector(-1400.0f, -60.0f, 375.0f));
			}
		}
		SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, 180.0f));
		FTimerHandle time;
		GetWorldTimerManager().SetTimer(time, this, &ATile::ResetIsTrap, 0.25f, false);
		GetWorldTimerManager().SetTimer(thTime, this, &ATile::Explode, fTrapTime, false);
	}
}

/*
void ATile::ExplodeWithDelay(){
	Explode();
	GetWorldTimerManager().SetTimer(thDelay, this, &ATile::Delay, fDelayTime, false);
}
*/

void ATile::ResetIsTrap(){
	//isTrap = false;
	SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, 230.0f));
}