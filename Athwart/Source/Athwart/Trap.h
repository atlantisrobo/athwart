// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Trap.generated.h"

UCLASS()
class ATHWART_API ATrap : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATrap();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void Explode();

protected:
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	class UAnimMontage* DeathAnim;

	UPROPERTY(VisibleAnywhere, Category = Collision)
	class USphereComponent* Sphere;
	
	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class UMeshComponent* Mesh;
};
