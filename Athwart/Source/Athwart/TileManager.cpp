// Fill out your copyright notice in the Description page of Project Settings.

#include "Athwart.h"
#include "Tile.h"
#include "EngineUtils.h"
#include "TileManager.h"


// Sets default values
ATileManager::ATileManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATileManager::BeginPlay()
{
	Super::BeginPlay();
	int x = 0;
	int y = 0;

	float tx = 550.0f;
	float ty = -760.0f;
	for (TActorIterator<ATile> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		tGrid[x][y] = *ActorItr;
		tGrid[x][y]->SetActorLocation(FVector(tx, ty, 230.0f));
		if (x == 8){
			x = 0;
			ty = -760.0f;
			tx += -200.0f;
			y++;
		}
		else{
			x++;
			ty += 200.0f;
		}
	}

	PresetOne(2.0f);
}

// Called every frame
void ATileManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ATileManager::PresetOne(float time)
{
	//Row One
	for (int x = 0; x < 3; x++){
		for (int y = 0; y < 3; y++){
			tGrid[x][y]->SetAsTrap(time, 0);
		}
	}

	for (int x = 3; x < 6; x++){
		for (int y = 0; y < 3; y++){
			tGrid[x][y]->SetAsTrap(time, time / 2);
		}
	}

	for (int x = 6; x < 9; x++){
		for (int y = 0; y < 3; y++){
			tGrid[x][y]->SetAsTrap(time, 0);
		}
	}

	//Row Two
	for (int x = 0; x < 3; x++){
		for (int y = 3; y < 6; y++){
			tGrid[x][y]->SetAsTrap(time, time / 2);
		}
	}

	for (int x = 3; x < 6; x++){
		for (int y = 3; y < 6; y++){
			tGrid[x][y]->SetAsTrap(time, 0);
		}
	}

	for (int x = 6; x < 9; x++){
		for (int y = 3; y < 6; y++){
			tGrid[x][y]->SetAsTrap(time, time / 2);
		}
	}

	//Row Three
	for (int x = 0; x < 3; x++){
		for (int y = 6; y < 9; y++){
			tGrid[x][y]->SetAsTrap(time, 0);
		}
	}

	for (int x = 3; x < 6; x++){
		for (int y = 6; y < 9; y++){
			tGrid[x][y]->SetAsTrap(time, time/2);
		}
	}

	for (int x = 6; x < 9; x++){
		for (int y = 6; y < 9; y++){
			tGrid[x][y]->SetAsTrap(time, 0);
		}
	}
}
void ATileManager::PresetTwo(float time)
{
	float offset = 0;
	for (int y = 8; y >= 0; y--){
		for (int x = 0; x < 9; x++){
			tGrid[x][y]->SetAsTrap(time, offset);
		}
		offset += 0.25f;
	}
}
void ATileManager::PresetThree(float time)
{
	float offset = 0;
	for (int y = 0; y < 9; y++){
		for (int x = 0; x < 9; x++){
			tGrid[x][y]->SetAsTrap(time, offset);
		}
		offset += 0.5f;
	}
}
void ATileManager::PresetFour(float time)
{
	for (int x = 0; x < 3; x++){
		for (int y = 0; y < 9; y++){
			tGrid[x][y]->SetAsWall();
		}
	}
	tGrid[3][4]->SetAsEmpty();

	for (int x = 6; x < 9; x++){
		for (int y = 0; y < 9; y++){
			tGrid[x][y]->SetAsWall();
		}
	}
	tGrid[6][4]->SetAsEmpty();

	float offset = 0;
	for (int y = 0; y < 9; y++){
		for (int x = 3; x < 6; x++){
			tGrid[x][y]->SetAsTrap(time, offset);
		}
		offset += 0.2f;
	}

}



void ATileManager::GeneratePath(){
	int start_x = FMath::RandRange(0,8);
	int last_state = 0;
	int last_x = start_x;
	for (int y = 0; y < 9; y++){
		switch (last_state){
		case 0:
			for (int x = 0; x < 9; x++){
				if (x == last_x){
					tGrid[x][y]->SetAsEmpty();
				}
				else{
					tGrid[x][y]->SetAsWall();
				}
			}
			last_state = 1;
			break;
		case 1:
			bool dir = FMath::RandRange(0, 1);
			int end_x;
			if (dir){
				end_x = FMath::RandRange(last_x, 8);
				last_state = 2;
			}
			else{
				end_x = FMath::RandRange(0, last_x);
				last_state = 3;
			}
			for (int x = 0; x < 9; x++){
				if (dir){
					if (x>=last_x && x<=end_x){
						tGrid[x][y]->SetAsEmpty();
					}
					else{
						tGrid[x][y]->SetAsWall();
					}
				}
				else{
					if (x <= last_x && x>=end_x){
						tGrid[x][y]->SetAsEmpty();
					}
					else{
						tGrid[x][y]->SetAsWall();
					}
				}
			}
			break;
		case 2:


			if (y == 7){
				last_state = 0;
			}
			break;

		case 3:


			if (y==7){
				last_state = 0;
			}
			break;
		}
		
	}
}