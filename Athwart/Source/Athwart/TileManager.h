// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Tile.h"
#include "TileManager.generated.h"

UCLASS()
class ATHWART_API ATileManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void SetTileTrap(int index);
	void SetTileTrap(int x, int y);

	void SetTileWall(int index);
	void SetTileWall(int x, int y);

	void ResetTile(int index);
	void ResetTile(int x, int y);

	void PresetOne(float time);
	void PresetTwo(float time);
	void PresetThree(float time);
	void PresetFour(float time);

	void GeneratePath();
private:
	void IndexToCoordinates(int index, int& x, int& y);
	int CoordinatesToIndex(int x, int y);
	class ATile* tGrid[9][9];
	
};
