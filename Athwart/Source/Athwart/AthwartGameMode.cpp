// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Athwart.h"
#include "AthwartGameMode.h"
#include "AthwartPlayerController.h"
#include "AthwartCharacter.h"

AAthwartGameMode::AAthwartGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AAthwartPlayerController::StaticClass();

	// Blueprinted Version, relies on the asset path obtained from the editor
	static ConstructorHelpers::FClassFinder<AAthwartPlayerController> VictoryPCOb(TEXT("AAthwartPlayerController'/Game/Blueprints/BP_AthwartPlayerController'"));
	if (VictoryPCOb.Class != NULL)
	{
		PlayerControllerClass = VictoryPCOb.Class;
	}
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AAthwartGameMode::BeginPlay()
{
	Super::BeginPlay();
	ChangeMenuWidget(StartingWidgetClass);
}

void AAthwartGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}
	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}

void AAthwartGameMode::setCurrentWidget(UUserWidget* widget)
{
	CurrentWidget = widget;
}