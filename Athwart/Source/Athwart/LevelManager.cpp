// Fill out your copyright notice in the Description page of Project Settings.

#include "Athwart.h"
#include "LevelManager.h"
#include "EngineUtils.h"
#include "TileManager.h"


// Sets default values
ALevelManager::ALevelManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	numLevel = 1;
}

// Called when the game starts or when spawned
void ALevelManager::BeginPlay()
{
	Super::BeginPlay();
	for (TActorIterator<ATileManager> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		TM = *ActorItr;
	}
	TM->PresetOne(2.0f);
}

// Called every frame
void ALevelManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ALevelManager::NextStage(){

}