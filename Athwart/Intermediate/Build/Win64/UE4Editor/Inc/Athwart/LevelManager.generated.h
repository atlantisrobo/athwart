// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ATHWART_LevelManager_generated_h
#error "LevelManager.generated.h already included, missing '#pragma once' in LevelManager.h"
#endif
#define ATHWART_LevelManager_generated_h

#define Athwart_Source_Athwart_LevelManager_h_11_RPC_WRAPPERS
#define Athwart_Source_Athwart_LevelManager_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Athwart_Source_Athwart_LevelManager_h_11_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesALevelManager(); \
	friend ATHWART_API class UClass* Z_Construct_UClass_ALevelManager(); \
	public: \
	DECLARE_CLASS(ALevelManager, AActor, COMPILED_IN_FLAGS(0), 0, Athwart, NO_API) \
	DECLARE_SERIALIZER(ALevelManager) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<ALevelManager*>(this); }


#define Athwart_Source_Athwart_LevelManager_h_11_INCLASS \
	private: \
	static void StaticRegisterNativesALevelManager(); \
	friend ATHWART_API class UClass* Z_Construct_UClass_ALevelManager(); \
	public: \
	DECLARE_CLASS(ALevelManager, AActor, COMPILED_IN_FLAGS(0), 0, Athwart, NO_API) \
	DECLARE_SERIALIZER(ALevelManager) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<ALevelManager*>(this); }


#define Athwart_Source_Athwart_LevelManager_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALevelManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALevelManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALevelManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALevelManager); \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ALevelManager(const ALevelManager& InCopy); \
public:


#define Athwart_Source_Athwart_LevelManager_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API ALevelManager(const ALevelManager& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALevelManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALevelManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALevelManager)


#define Athwart_Source_Athwart_LevelManager_h_8_PROLOG
#define Athwart_Source_Athwart_LevelManager_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Athwart_Source_Athwart_LevelManager_h_11_RPC_WRAPPERS \
	Athwart_Source_Athwart_LevelManager_h_11_INCLASS \
	Athwart_Source_Athwart_LevelManager_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Athwart_Source_Athwart_LevelManager_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Athwart_Source_Athwart_LevelManager_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Athwart_Source_Athwart_LevelManager_h_11_INCLASS_NO_PURE_DECLS \
	Athwart_Source_Athwart_LevelManager_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Athwart_Source_Athwart_LevelManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
